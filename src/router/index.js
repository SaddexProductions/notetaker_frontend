import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import NotFound from '../views/NotFound.vue'
import Splash from '../views/Splash.vue'
import Login from '../views/Login.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Dashboard',
    component: Home,
    meta: {
      title: 'The Notetaker - Dashboard'
    }
  },
  {
    path: '/splash',
    name: 'Splash',
    component: Splash,
    meta: {
      title: 'The Notetaker'
    }
  },
  {
    path: '/edit',
    name: 'Edit',
    meta: {
      title: 'Editing new - The Notetaker'
    },
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Edit.vue')
  },
  {
    path: '/edituser',
    name: 'Settings',
    meta: {
      title: 'Settings - The Notetaker'
    },
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Settings.vue')
  },
  {
    path: '/view',
    name: 'View',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/View.vue')
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  { path: '*', component: NotFound }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach(async (to, from, next) => {
  await Vue.nextTick()
  const { authLoaded, isAuth, offline, offlineLoaded } = router.app.$children[0].$data
  const { autoLogin, setAuth, setAuthLoaded } = router.app.$children[0]

  let auth = isAuth

  let oL = offline

  if (!authLoaded) auth = await autoLogin()
  setAuthLoaded()
  setAuth(auth)

  if (!offlineLoaded) {
    if (localStorage.getItem('offline')) {
      oL = true
    }
  }

  if (!auth && !oL && to.name !== 'Splash' && to.name !== 'Login') {
    return router.replace('/splash')
  } else if ((auth && to.name === 'Splash') || (oL && to.name === 'Splash') || (auth && to.name === 'Login')) {
    return router.replace('/')
  }

  const nearestWithTitle = to.matched.slice().reverse().find(r => r.meta && r.meta.title)

  // If a route with a title was found, set the document (page) title to that value.
  if (nearestWithTitle) document.title = nearestWithTitle.meta.title

  next()
})

export default router
