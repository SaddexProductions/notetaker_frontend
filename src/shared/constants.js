//constants
export const desc = 'Must contain at least one lowercase letter, one uppercase letter and one number';
export const pW = /^(?:(?=.*[a-z])(?:(?=.*[A-Z])(?=.*[\d\W])|(?=.*\W)(?=.*\d))|(?=.*\W)(?=.*[A-Z])(?=.*\d)).{5,25}$/;
export const twoFA = {
  config: {
    category: "checkbox"
  },
  valid: true,
  touched: true, //left to ensure that easy setting works
  title: "Two-factor authentication (Authy) ",
  id: "twoFactorAuth", //must be same as key name
  value: false,
  help: {
    steps: [
      {
        desc: "Download and install Authy",
        link: true,
        url: "https://authy.com/download/",
        img: "qrcode.jpeg"
      },
      {
        desc: 'Open the app'
      },
      {
        desc: "Click on Notetaker"
      },
      {
        desc: 'Enter the token when logging in'
      }
    ],
    or: [
      {
        desc: 'Request an SMS token when logging in'
      },
      {
        desc: 'Enter the code from the SMS'
      }
    ]
  },
};

export const cellphone = {
  config: {
    category: "input",
    type: "text",
    placeholder: "123 456 789 0",
    maxLength: 25
  },
  doNotRender: true,
  valid: true,
  touched: false,
  validation: {
    required: true,
    trim: true,
    minLength: 6,
    maxLength: 20,
    regex: /^([0-9][0-9][0-9])\W*([0-9][0-9]{2})\W*([0-9]{0,5})$/
  },
  id: "cellphone", //must be same as key name
  title: "Phone number",
  value: "",
  expand: true,
  offset: 1,
  extra: {
    type: "country code",
    value: ""
  }
}