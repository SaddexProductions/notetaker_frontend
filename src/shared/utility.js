//methods
export const checkValid = (val, rules, extra = false) => {
  let isValid = true

  if (!rules) return isValid

  if (rules.trim) val = val.replace(/\s/g, '');

  if (rules.required && !rules.isArray) {
    isValid = val.trim() !== '' && isValid
  }
  else if (rules.required && rules.isArray) {
    isValid = val !== '' && isValid
  }
  if (rules.minLength && !rules.partOfArray && val.length > 0 || rules.minLength && !rules.partOfArray && rules.required) {
    isValid = val.length >= rules.minLength && isValid
  }
  else if (rules.minLength && rules.partOfArray) {
    isValid = val.trim().replace(',', '').length >= rules.minLength && isValid || val.length === 0 && !rules.required && isValid
  }
  if (rules.maxLength) {
    isValid = val.length <= rules.maxLength && isValid
  }

  if (rules.isEmail) {
    const pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    isValid = pattern.test(val) && isValid
  }
  if (rules.isNumeric) {
    const pattern = /^\d+$/
    isValid = pattern.test(val) && isValid
  }
  if (rules.regex && !rules.partOfArray && rules.required || rules.regex && !rules.partOfArray && val.length > 0) {
    isValid = rules.regex.test(val) && isValid
  }
  else if (rules.regex && rules.partOfArray) {
    isValid = rules.regex.test(val.replace(',', '')) && isValid
  }

  if (rules.isArray) {
    for (let key of val) {
      isValid = checkValid(key, rules.rules) && isValid
    }
  }
  if (rules.unique) {
    const arrayToLowerCase = [...extra.items].join("-").toLowerCase().split('-');
    if (rules.testOne) {
      //checks that the value in the tags edit field don't match an existing tag
      const lowerCaseVal = val.toLowerCase().replace(',', '');
      for (let e of arrayToLowerCase) {
        isValid = e !== lowerCaseVal && isValid
      }
    }
    else {
      //checks that there aren't any duplicates when localstorage loads
      const comparasionArray = [...extra.items].join("-").toLowerCase().split('-');
      for (let e = 0; e < arrayToLowerCase.length; e++) {
        comparasionArray.splice(e, 1);
        for (let f of comparasionArray) {
          isValid = arrayToLowerCase[e] !== f && isValid
        }
        arrayToLowerCase.splice(e, 1);
      }
    }
  }
  if (rules.isDate) {
    //check if date string has the matching format
    const regEx = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}$/;
    isValid = regEx.test(val) && isValid
    if (isValid) {
      //if date has a valid format, checks whether it's a valid date
      const d = new Date(val);
      const dNum = d.getTime();
      if (!dNum && dNum !== 0) isValid = false
    }
  }
  if (rules.mustMatch) {
    isValid = val === extra.password.value && isValid
  }
  return isValid
}

export const checkEmpty = obj => {
  let allEmpty = true
  for (const key in obj) {
    if (obj[key].value !== '' || obj[key].items && obj[key].items.length > 0) allEmpty = false
  }

  return allEmpty
}

export const dateFilter = val => {
  if (val) {
    return val.split('T').join(' ').split(':').splice(0, 2).join(':')
  }
  else {
    return ''
  }
}

export const deletePromptGenerator = (obj, notes, numberOfItemsToDelete) => {
  let desc = numberOfItemsToDelete + " items";

  if (!obj && numberOfItemsToDelete < 2) {
    for (let note of notes) {
      if (note.markedForDeletion) desc = 'the note "' + note.title + '"';
    }
  } else if (obj) {
    desc = desc = 'the note "' + obj.title + '"';
  }
  return "Are you sure you want to delete " + desc + "?";
}

export const validateForm = obj => {
  let formIsValid = true;
  for (let key in obj) {
    if (!obj[key].valid) formIsValid = false;
  }
  return formIsValid
}

export const setFields = (target, obj, setTouched) => {
  let str = target.value;

  if (obj.content[target.id].expand) {
    str = splitNumber(target.value, obj.content[target.id]);
  }

  obj.content[target.id] = {
    ...obj.content[target.id],
    value: str,
    touched: setTouched ? true : obj.content[target.id].touched,
    valid: checkValid(
      str,
      obj.content[target.id].validation,
      obj.content
    )
  };

  if(target.id === "password" && obj.content.repeatPassword) {
    obj.content.repeatPassword.valid = 
    checkValid(obj.content.repeatPassword.value, obj.content.repeatPassword.validation, {
        ...obj.content,
        password: {
          ...obj.content.password
        }
    });
}

  if (target.id === "twoFactorAuth") {
    obj.content = {
      ...obj.content,
      cellphone: {
        ...obj.content.cellphone,
        doNotRender: !obj.content.twoFactorAuth.value,
        validation: {
          ...obj.content.cellphone.validation
        },
        valid: obj.content.twoFactorAuth.value > 0
          ? checkValid(
            obj.content.cellphone.value,
            obj.content.cellphone.validation
          )
          : true
      }
    };
  }
  return obj;
}

export const splitNumber = (inputString, obj) => {
  let str;
  if (obj.value.length <= inputString.length) {
    str = inputString.replace(/\s/g, '').split('');
    const objOffset = obj.offset || 0;
    for (let i = 0; i < str.length; i++) {
      if ((i + objOffset) % 4 === 0 && str[i] !== " " && i !== 0) {
        str.splice(i, 0, ' ');
      }
    }
  }
  else {
    str = inputString.split('');
    if (str[str.length - 1] === ' ') str.splice(str.length - 1, 1);
  }
  str = str.join('');

  return str;
}

