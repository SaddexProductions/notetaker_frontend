import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import VueCountryCode from './shared/vue-country-code';
import { library } from '@fortawesome/fontawesome-svg-core'
import {
  faUser,
  faUserEdit,
  faSignOutAlt,
  faFileMedical,
  faFileAlt,
  faTh,
  faList,
  faSearch,
  faPlus,
  faTrash,
  faEdit,
  faArrowUp,
  faArrowDown,
  faExclamationTriangle,
  faQuestionCircle,
  faCheckCircle,
  faFileUpload,
  faEllipsisH
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import BgCredit from './components/bgCredit/BgCredit.vue'
import CheckBox from './components/checkBox/Checkbox.vue'
import Loader from './components/loader/Loader.vue'
import Modal from './components/modal/Modal.vue'

library.add([faUser,
  faUserEdit,
  faSignOutAlt,
  faFileMedical,
  faFileAlt,
  faTh,
  faList,
  faSearch,
  faPlus,
  faTrash,
  faEdit,
  faArrowUp,
  faArrowDown,
  faExclamationTriangle,
  faQuestionCircle,
  faCheckCircle,
  faFileUpload,
  faEllipsisH
])

Vue.component('bgcredit', BgCredit)
Vue.component('checkbox', CheckBox)
Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.component('modal', Modal)
Vue.component('loader', Loader)

Vue.config.productionTip = false

Vue.use(VueCountryCode)

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
