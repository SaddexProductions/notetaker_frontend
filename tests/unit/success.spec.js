import Success from '../../src/components/success/Success.vue';
import {mount, shallowMount} from '@vue/test-utils';

describe('Success.vue', () => {

    let wrapper;

    beforeEach(() => {
        wrapper = shallowMount(Success, {propsData: {
            message: "Hello World!"
        }});
    });

    it("renders", () => {
        expect(wrapper.exists()).toBeTruthy();
    });

    it("renders a p tag with the message 'Hello World!", () => {
        expect(wrapper.find("p").text()).toBe("Hello World!");
    });

    it('matches snapshot', () => {
        const wrapperSnap = mount(Success);
        expect(wrapperSnap.element).toMatchSnapshot();
    });
});