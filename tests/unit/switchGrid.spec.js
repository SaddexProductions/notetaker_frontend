import SwitchGrid from '../../src/components/switchGridButtons/SwitchGrid.vue';
import {mount, shallowMount} from '@vue/test-utils';

describe("SwitchGrid.vue", () => {
    let wrapper;

    beforeEach(() => {
        wrapper = shallowMount(SwitchGrid, {propsData: {
            grid: false,
            setGrid: jest.fn()
        }});
    });

    it("renders", () => {
        expect(wrapper.exists()).toBeTruthy();
    });

    it("renders list button with class active when grid is false", () => {
        expect(wrapper.find(".list").html()).toMatch(/list active/);
    });

    it("renders grid button with class active when grid is true", () => {
        const gridTrueWrapper = shallowMount(SwitchGrid, {propsData: {
            grid: true,
            setGrid: jest.fn()
        }})
        expect(gridTrueWrapper.find(".grid").html()).toMatch(/grid active/);
    });

    it('matches snapshot', () => {
        const wrapperSnap = mount(SwitchGrid);
        expect(wrapperSnap.element).toMatchSnapshot();
    });
});