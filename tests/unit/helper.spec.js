import Helper from '../../src/components/helper/Helper.vue';
import { mount, shallowMount } from '@vue/test-utils';

const propsData = {
    content: {
        steps: [
            {
                desc: "Download and install Authy",
                link: true,
                url: "https://authy.com/download/",
                img: "qrcode.jpeg"
            },
            {
                desc: 'Open the app',
                link: true,
                url: "https://saddexproductions.com"
            },
            {
                desc: "Click on Notetaker"
            },
            {
                desc: 'Enter the token when logging in'
            }
        ],
        or: [
            {
                desc: 'Request an SMS token when logging in'
            },
            {
                desc: 'Enter the code from the SMS'
            }
        ]
    }
};

describe("Helper.vue", () => {

    let wrapper;

    beforeEach(() => {
        wrapper = shallowMount(Helper, {
            propsData 
        });
    });

    it("renders", () => {
        expect(wrapper.exists()).toBeTruthy();
    });

    it("contains one <img> tag if one of the prop content.steps has an img property", () => {
        expect(wrapper.findAll("img")).toHaveLength(1);
    });

    it("the image tag shows up in the first li element if the first props.content.step has an img property", () => {
        expect(wrapper.find("#helper-step-0 img").exists()).toBeTruthy();
    });

    it("contains two <a> tags if two of the prop content.steps has a link property", () => {
        expect(wrapper.findAll("a")).toHaveLength(2);
    });

    it("contains an ol element if prop content.or is truthy", () => {
        expect(wrapper.find("ol").exists()).toBeTruthy();
    });

    it('matches snapshot', () => {
        const wrapperSnap = mount(Helper, {propsData});
        expect(wrapperSnap.element).toMatchSnapshot();
    });
});