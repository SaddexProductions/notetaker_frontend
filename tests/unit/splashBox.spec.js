import SplashBox from '../../src/components/splashbox/SplashBox.vue';
import {mount, shallowMount} from '@vue/test-utils';

describe("SplashBox.vue", () => {

    let wrapper;

    beforeEach(() => {
        wrapper = shallowMount(SplashBox, {propsData: {
            h3title: "Hello World!",
            wider: false
        }});
    });

    it("renders", () => {
        expect(wrapper.exists()).toBeTruthy();
    });

    it("renders a h3 tag with the text 'Hello World!'", () => {
        expect(wrapper.find("h3").text()).toBe("Hello World!");
    });

    it("renders the div sBox with the class wider if props.wider is true", () => {
        const widerTrueWrapper = shallowMount(SplashBox, {propsData: {
            h3title: "Hello World!",
            wider: true
        }});

        expect(widerTrueWrapper.find(".sBox").html()).toMatch(/sBox wider/);
    });

    it("renders the div sBox without the class wider if props.wider is false", () => {
        expect(wrapper.find(".sBox").html()).not.toMatch(/sBox wider/)
    });

    it('matches snapshot', () => {
        const wrapperSnap = mount(SplashBox);
        expect(wrapperSnap.element).toMatchSnapshot();
    });
});