import Sort from '../../src/components/sort/Sort.vue';
import {mount, shallowMount} from '@vue/test-utils';

const propsData = {
    selected: "title",
    showNew: false,
    sortItems: jest.fn(),
    sortUp: false,
    toggleOrder: jest.fn()
};

describe("Sort.vue", () => {

    let standard;

    beforeEach(() => {
        standard = shallowMount(Sort, {propsData});
    });

    it("renders", () => {
        expect(standard.exists()).toBeTruthy();
    });

    it("renders fa-arrow-down when sortUp is false", () => {
        expect(standard.find(".ui-button").html()).toMatch(/icon=\"arrow-down\"/);
    });

    it("renders fa-arrow-up when sortUp is true", () => {
        const arrowUpWrapper = shallowMount(Sort, {propsData: {
            ...propsData,
            sortUp: true
        }});
        expect(arrowUpWrapper.find(".ui-button").html()).toMatch(/icon=\"arrow-up\"/);
    });

    it('matches snapshot', () => {
        const wrapperSnap = mount(Sort);
        expect(wrapperSnap.element).toMatchSnapshot();
    });

});