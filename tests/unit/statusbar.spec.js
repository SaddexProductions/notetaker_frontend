import StatusBar from '../../src/components/statusBar/StatusBar.vue';
import {mount, shallowMount} from '@vue/test-utils';

describe("StatusBar.vue", () => {

    let wrapper;

    beforeEach(() => {
        wrapper = shallowMount(StatusBar, {propsData: {
            status: {
                active: true,
                message: "Hello World!",
                error: false
            }
        }});
    });

    it("renders", () => {
        expect(wrapper.exists()).toBe(true);
    });

    it("does not render the div statusBar if the prop active is false", () => {
        const activeFalseWrapper = shallowMount(StatusBar, {propsData: {
            status: {
                active: false,
                message: "Hello World!",
                error: false
            }
        }});

        expect(activeFalseWrapper.find(".statusBar").exists()).toBe(false);
    });

    it("renders the div statusBar if the prop active is true", () => {
        expect(wrapper.find(".statusBar").exists()).toBeTruthy();
    });

    it("renders the text 'Hello World!'", () => {
        expect(wrapper.find("h2").text()).toMatch(/Hello World!/);
    });

    it("renders a font-awesome icon 'check-circle' inside the h2 tag if prop status.error is false", () => {
        expect(wrapper.find("h2").html()).toMatch(/icon=\"check-circle\"/);
    });

    it("renders a font-awesome icon 'exclaimation-triangle' inside the h2 tag if prop status.error is true", () => {

        const errorTrueWrapper = shallowMount(StatusBar, {propsData: {
            status: {
                active: true,
                message: "Hello World!",
                error: true
            }
        }});

        expect(errorTrueWrapper.find("h2").html()).toMatch(/icon=\"exclaimation-triangle\"/);
    });

    it('matches snapshot', () => {
        const wrapperSnap = mount(StatusBar, {propsData: {
            status: {
                active: false,
                message: "Hello World!",
                error: false
            }
        }});
        expect(wrapperSnap.element).toMatchSnapshot();
    });
});