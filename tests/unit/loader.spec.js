import Loader from '../../src/components/loader/Loader.vue';
import {mount, shallowMount} from '@vue/test-utils';

describe("Loader.vue", () => {

    let wrapper;

    beforeEach(() => {
        wrapper = shallowMount(Loader, {propsData: {
            color: "green"
        }});
    });

    it("renders", () => {
        expect(wrapper.exists()).toBeTruthy();
    });

    it("renders a div with the classes loader green if prop color is set to 'green'", () => {
        expect(wrapper.find("div").html()).toMatch(/loader green/);
    });

    it('matches snapshot', () => {
        const wrapperSnap = mount(Loader);
        expect(wrapperSnap.element).toMatchSnapshot();
    });
});