import Select from '../../src/components/select/Select.vue';
import {mount, shallowMount} from '@vue/test-utils';

describe("Select.vue", () => {
    let wrapper;

    beforeEach(() => {
        wrapper = shallowMount(Select, {propsData: {
            selectValue: "title",
            sortItems: jest.fn(),
            search: true
        }});
    });

    it("renders", () => {
        expect(wrapper.exists()).toBeTruthy();
    });

    it("renders select tag with relevance option if prop search is true", () => {
        expect(wrapper.find("select").text()).toMatch(/Relevance/);
    });

    it("renders select tag without relevance option if prop search is false", () => {

        const searchFalseWrapper = shallowMount(Select, {propsData: {
            selectValue: "title",
            sortItems: jest.fn(),
            search: false
        }});

        expect(searchFalseWrapper.find("select").text()).not.toMatch(/Relevance/);
    });

    it('matches snapshot', () => {
        const wrapperSnap = mount(Select);
        expect(wrapperSnap.element).toMatchSnapshot();
    });

});