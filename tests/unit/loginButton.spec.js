import LoginButton from '../../src/components/loginButton/LoginButton.vue';
import {mount, shallowMount} from '@vue/test-utils';

const propsData = {
    isDisabled: false,
    text: "Hello World!",
    noSubmit: false
}

describe("LoginButton.vue", () => {

    let wrapper;

    beforeEach(() => {
        wrapper = shallowMount(LoginButton, {propsData });
    });

    it("renders", () => {
        expect(wrapper.exists()).toBeTruthy();
    });

    it("renders a button with the text 'Hello World!'", () => {
        expect(wrapper.find("button").text()).toMatch(/Hello World!/);
    });

    it("renders a button with type submit if prop noSubmit is false", () => {
        expect(wrapper.find("button").html()).toMatch(/type=\"submit\"/);
    });

    it("renders a button with the type 'button' if prop noSubmit is true", () => {
        const wrapperNoSubmitTrue = shallowMount(LoginButton, {propsData: {...propsData,
        noSubmit: true}});
        expect(wrapperNoSubmitTrue.find("button").html()).toMatch(/type=\"button\"/);
    });

    it("renders a disabled button if prop isDisabled is true", () => {
        const buttonDisabledTrueWrapper = shallowMount(LoginButton, {propsData: {...propsData,
        isDisabled: true}});

        expect(buttonDisabledTrueWrapper.find("button").html()).toMatch(/disabled/);
    });

    it('matches snapshot', () => {
        const wrapperSnap = mount(LoginButton);
        expect(wrapperSnap.element).toMatchSnapshot();
    });
});