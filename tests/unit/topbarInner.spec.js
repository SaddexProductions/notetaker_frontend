import TopbarInner from '../../src/components/topbarInner/TopbarInner.vue';
import { mount, shallowMount } from '@vue/test-utils';

describe("TopbarInner.vue", () => {

    let wrapper;

    beforeEach(() => {
        wrapper = shallowMount(TopbarInner, {
            propsData: {
                expand: false
            }
        });
    });

    it("renders", () => {
        expect(wrapper.exists()).toBeTruthy();
    });

    it("should not have expand class if expand prop is false", () => {
        expect(wrapper.find(".topBar").html()).not.toMatch(/expand/);
    });

    it("should have expand class if expand prop is true", () => {
        const customWrapper = shallowMount(TopbarInner, {
            propsData: {
                expand: true
            }
        });
        expect(customWrapper.find(".topBar").html()).toMatch(/expand/);
    });


    it('matches snapshot', () => {
        const wrapperSnap = mount(TopbarInner);
        expect(wrapperSnap.element).toMatchSnapshot();
    });
});