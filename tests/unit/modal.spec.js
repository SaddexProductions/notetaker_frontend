import Modal from '../../src/components/modal/Modal.vue';
import {mount, shallowMount} from '@vue/test-utils';

const propsData = {
    message: "Hello World!",
    error: false
};

describe("Modal.vue", () => {

    let wrapper;

    beforeEach(() => {
        wrapper = shallowMount(Modal, {propsData});
    });

    it("renders", () => {
        expect(wrapper.exists()).toBeTruthy();
    });

    it("renders a p tag with the message 'Hello World!'", () => {
        expect(wrapper.find("p").text()).toMatch(/Hello World!/);
    });

    it("renders a h3 tag with the text 'Warning' if prop error is false", () => {
        expect(wrapper.find("h3").text()).toMatch(/Warning/);
    });

    it("renders a h3 tag with the text 'Error' if prop error is true", () => {
        const errorTrueWrapper = shallowMount(Modal, {propsData: {
            message: "",
            error: true
        }}); 

        expect(errorTrueWrapper.find("h3").text()).toMatch(/Error/);
    });

    it("renders a ';' symbol in the p tag if prop error.items exists", () => {
        const errorItemsPropData = {...propsData, error: {
            items: ["example"]
        }};

        const propsErrorItemsTrueWrapper = shallowMount(Modal, {propsData: errorItemsPropData});
        expect(propsErrorItemsTrueWrapper.find("p").text()).toMatch(/;/);
    });

    it("does not render an ul tag if prop error.items is absent", () => {
        expect(wrapper.find("ul").exists()).toBeFalsy();
    });
    
    it('renders an ul tag if prop error.items exists', () => {
        const errorItemsPropData = {...propsData, error: {
            items: ["example"]
        }};

        const propsErrorItemsTrueWrapper = shallowMount(Modal, {propsData: errorItemsPropData});
        expect(propsErrorItemsTrueWrapper.find("ul").text()).toBeTruthy();
    });

    it("renders 2 li tags if prop error.items exists and contains 2 elements", () => {
        const errorItemsPropData = {...propsData, error: {
            items: ["example", "example2"]
        }};

        const propsErrorItemsTrueWrapper = shallowMount(Modal, {propsData: errorItemsPropData});
        expect(propsErrorItemsTrueWrapper.findAll("li")).toHaveLength(2);
    });

    it('matches snapshot', () => {
        const wrapperSnap = mount(Modal);
        expect(wrapperSnap.element).toMatchSnapshot();
    });
});