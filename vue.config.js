module.exports = {
  chainWebpack: config => {
    config.module.rules.delete('eslint')
  },
  css: {
    loaderOptions: {
      sass: {
        prependData: `
              @import "@/shared/scss/_variables.scss";
              @import "@/shared/scss/_mixins.scss";
            `
      }
    }
  }
}
